import { describe } from "mocha";
import climbing_ladders from "../src/complementary/climbing-ladders.js";
import { expect, should } from "chai";

describe("climbing-ladders.js", () => {
    it("all essential functions exists", () => {
        should().exist(
            climbing_ladders.fibonacci, 
            climbing_ladders.check_n_correctness, 
            climbing_ladders.distinct_ways
            );
    });
    it("returns correct result", () => {
        expect(climbing_ladders.distinct_ways(5) === 8);
        expect(climbing_ladders.distinct_ways(15) === 987);
    });
    it("negative number throws an error", () => {
        const err_msg = "passed value 'n' must be positive number";
        expect(() => climbing_ladders.distinct_ways(-1))
            .to
            .throw(err_msg);
    });
    it("wrong datatype throws an error", () => {
        const err_msg = "passed value 'n' must be number type";
        expect(() => climbing_ladders.distinct_ways("test"))
            .to
            .throw(err_msg);
    });
    it("decimal number throws an error", () => {
        const err_msg = "passed value 'n' must be whole number";
        expect(() => climbing_ladders.distinct_ways(1.2))
            .to
            .throw(err_msg);
    });
});

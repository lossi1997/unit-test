import { describe } from "mocha";
import bubblesort from "../src/complementary/bubblesort.js"
import { expect, should } from "chai";

describe("bubblesort.js", () => {
    it("function exists", () => {
        should().exist(bubblesort.sort);
    });
    it("returns correct result", () => {
        expect(bubblesort.sort([5, 4, 3, 2, 1]) == [1, 2, 3, 4, 5]);
        expect(bubblesort.sort([4, 5, 1, 3, 2]) == [1, 2, 3, 4, 5]);
    });
    it("passed other than array throws an error", () => {
        const err_msg = "passed value must be array";
        expect(() => bubblesort.sort("meow"))
            .to
            .throw(err_msg);
        expect(() => bubblesort.sort(4.4))
            .to
            .throw(err_msg);
        expect(() => bubblesort.sort(true))
            .to
            .throw(err_msg);
    });
});

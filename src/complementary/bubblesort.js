
// sort the passed array with bubblesort algorithm

const sort = (arr) => {
    if(Array.isArray(arr) == false) throw new Error("passed value must be array");
    var sorted = false;
    while(sorted == false) {
        var swaps = 0;
        for(var i = 0; i < arr.length; i++) {
            if(arr[i] > arr[i+1]) {
                var temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                swaps++;
            }
        }
        if (swaps == 0) {
            sorted = true;
        }
    }
    return arr;
};

export default { sort }

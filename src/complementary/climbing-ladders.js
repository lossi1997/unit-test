
// Find out how many distinct ways there is to climb to the top of a ladder containing 'n' steps

const fibonacci = (n, n1=0, n2=1) => {
    if(n == 0) {
        return n2;
    } else {
        return fibonacci(n-1, n2, n1+n2);
    }
};

const check_n_correctness = (n) => {
    if (n < 0) throw new Error("passed value 'n' must be positive number");
    if (typeof n !== "number") throw new Error("passed value 'n' must be number type");
    if (n % 1 !== 0) throw new Error("passed value 'n' must be whole number");
    return null;
};

const distinct_ways = (n) => {
    check_n_correctness(n);
    return fibonacci(n);
};

export default { fibonacci, check_n_correctness, distinct_ways }
